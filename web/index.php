<?php

require('../vendor/autoload.php');

$app = new Silex\Application();
$app['debug'] = true;

// Register the monolog logging service
$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => 'php://stderr',
));

// Register view rendering
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

function sendButton($payload, $command, $from_id) 
{
	$keyboard = [
		'one_time' => false,
		'buttons' => [
			[
				[
					"action" => [
					"type" => "text",
					"payload" => "{\"button\": \"${command}\"}",
					"label" => "Посты со мной",
					],
					"color" => "primary",
				]
			]
		]
	];

	$newMessage = [
		'user_id' => $from_id,
		'random_id' => rand(),
		'message' => 'Что ты хочешь получить?',
		'keyboard' => json_encode($keyboard, JSON_UNESCAPED_UNICODE),
		'access_token' => getenv('APP_KEY'),
		'v' => '5.80',
	];

	return $newMessage;
}

function getPosts($interference_id, $from_id, $test_id, $offset, $count) 
{

	$exists = false;

	//getting posts

	//getting posts of this group
	$group = [
		'owner_id' => -$interference_id,
		'access_token' => getenv('SERVICE_TOKEN'),
		'v' => '5.80',
		'count' => $count,
		'offset' => $offset,
	];

	$response = json_decode(
		file_get_contents('https://api.vk.com/method/wall.get?'.http_build_query($group))
	);

	$posts = $response->response->items;

	//temp for mentories of user
	$tmpMent = "/\[id$from_id\|.*\]/";


	foreach ($posts as $post) 
	{
		if(preg_match($tmpMent, $post->text))
		{
			$newMessage = [
				'user_id' => $from_id,
				'random_id' => rand(),
				'message' => '',
				'access_token' => getenv('APP_KEY'),
				'v' => '5.80',
				'attachment' => "wall-${interference_id}_{$post->id}",
			];

			file_get_contents('https://api.vk.com/method/messages.send?'.http_build_query($newMessage));
			$exists = true;
		}
	}

	if($offset + $count <= $response->response->count) {
		return getPosts($interference_id, $from_id, $test_id, $offset + $count, 100);
	}

	return $exists;
}

$app->get('/', function() use($app) {
	return 'Ok';
});

// Our web handlers

$app->post('/bot', function() use($app) {

	$data = json_decode(file_get_contents('php://input'));

	if(!$data)
		return 'bad';

	if($data->secret != getenv('APP_SECRET'))
		return 'bad';

	switch($data->type)
	{
		case 'confirmation':
			return getenv('CONFIRMATION_CODE');
			break;
		case 'message_new':

			$admin_id = getenv('ADMIN_ID');		//id of admin group
					
			$interference_id = getenv('INTERFERENCE_ID');		//id of interference
			$market_id = getenv('MARKET_ID');		//id of market

			$from_id = $data->object->from_id;
			$test_id = 216560043;

			if(isset($data->object->payload))
			{
				$payload = json_decode($data->object->payload);

				if(isset($payload->command)) 
				{

					$newMessage = sendButton($payload, 'posts', $from_id);
					file_get_contents('https://api.vk.com/method/messages.send?'.http_build_query($newMessage));

					return 'ok';
					break;
				} 

				if(isset($payload->button)) {

					switch ($payload->button) {
						case 'posts':

							$exists = getPosts($interference_id, $from_id, $test_id, 0, 20);

							//if matches dont exist
							if(!$exists)
							{
								$newMessage = [
									'user_id' => $from_id,
									'random_id' => rand(),
									'message' => "Не найдено постов с твоим участием. Ты можешь заказать индивидуальную съемку, написав [id$admin_id|Полине] или прямо сюда.",
									'access_token' => getenv('APP_KEY'),
									'v' => '5.80',
									'attachment' => "market-${interference_id}_{$market_id}",
								];

								file_get_contents('https://api.vk.com/method/messages.send?'.http_build_query($newMessage));
							}

							return 'ok';
							break;
					}


					return 'ok';
					break;
					
				}	
			}

			return 'ok';
			break;

		case 'group_leave':

			$admin_id = getenv('ADMIN_ID');		//id of admin group	
			$leaved_id = $data->object->user_id;		//user leaved group

			$messageToLeaved = [
				'user_id' => $leaved_id,
				'random_id' => rand(),
				'message' => "Почему отписался? Я тебя найду, пидор.",
				'access_token' => getenv('APP_KEY'),
				'v' => '5.80',
			];

			file_get_contents("https://api.vk.com/method/messages.send?".http_build_query($messageToLeaved));

			$messageToAdmin = [
				'user_id' => $admin_id,
				'random_id' => rand(),
				'message' => "Тут крыса покинула группу. [id$leaved_id|Вот она]",
				'access_token' => getenv('APP_KEY'),
				'v' => '5.80',
			];

			file_get_contents("https://api.vk.com/method/messages.send?".http_build_query($messageToAdmin));

			return 'ok';
			break;

		default:
			return 'ok';
	}

	return 'bad';
});

$app->run();
